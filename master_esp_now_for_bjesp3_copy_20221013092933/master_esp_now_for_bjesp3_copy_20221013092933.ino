
#include <ESP8266WiFi.h>
#include <espnow.h>

#define BUTTONPIN 2
 
// REPLACE WITH THE MAC Address of your receiver 
uint8_t broadcastAddress[] = {0x5C, 0xCF, 0x7F, 0x87, 0x60, 0x27};

//Structure example to send data
//Must match the receiver structure
typedef struct struct_message {
    bool state;
} struct_message;

// Create a struct_message called DHTReadings to hold sensor readings
struct_message switchState;

// Create a struct_message to hold incoming sensor readings
struct_message incomingReadings;
bool onOffState = false;

// Callback when data is sent
void OnDataSent(uint8_t *mac_addr, uint8_t sendStatus) {
  Serial.print("Last Packet Send Status: ");
  if (sendStatus == 0){
    Serial.println("Delivery success");
  }
  else{
    Serial.println("Delivery fail");
  }
}
 
void setup() {
  // Init Serial Monitor
  Serial.begin(9600);
 
  // Set device as a Wi-Fi Station
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();

  // Init ESP-NOW
  if (esp_now_init() != 0) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }

  // Set ESP-NOW Role
  esp_now_set_self_role(ESP_NOW_ROLE_CONTROLLER);

  memcpy(peerInfo.peer_addr, broadcastAddress, 6);
  peerInfo.channel = 0;  
  peerInfo.encrypt = false;
  
  // Add peer        
  if (esp_now_add_peer(&peerInfo) != ESP_OK){
    Serial.println("Failed to add peer");
    return;
  }

  // Once ESPNow is successfully Init, we will register for Send CB to
  // get the status of Trasnmitted packet
  esp_now_register_send_cb(OnDataSent);
  
  // Register peer
  esp_now_add_peer(broadcastAddress, ESP_NOW_ROLE_SLAVE, 1, NULL, 0);
  pinMode(BUTTONPIN, INPUT_PULLUP);
}

bool samePress = false;
void loop() {
  int buttonState = digitalRead(BUTTONPIN);
  if (!samePress && buttonState == LOW) {
    onOffState = !onOffState;
    samePress = true;
    switchState.state = onOffState;
    if (onOffState) {
      esp_now_send(broadcastAddress, (uint8_t *) &switchState, sizeof(switchState));
      Serial.println("Switch Activated: ON");
    }
    else {
      esp_now_send(broadcastAddress, (uint8_t *) &switchState, sizeof(switchState));
      Serial.println("Switch Activated: OFF");
    }
  }
  else if (samePress && buttonState != LOW) {
    samePress = false;
  }
}