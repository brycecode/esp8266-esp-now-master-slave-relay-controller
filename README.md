# ESP8266 ESP-NOW Master Slave Relay Controller

## Meta
Using two ESP8266-01 modules with ESP-NOW

Module 1: BJESP3 MAC: 18:FE:34:F4:50:57 MASTER  
Module 2: BJESP2 MAC: 5C:CF:7F:87:60:27 SLAVE

## Project
I want to be able to control my weed sprayer with a remote. I can use the WIFI
features of the ESP8266. ESP-NOW protocol allows for a master slave or even peer-to-peer topologies. In this case I will use the master slave feature. The
master will broadcast a message to specific MAC addresses. The slave can listen for those message. When a button is pressed master will send a message telling the slave to turn on or off a relay. Relay will hook to the weed sprayer.

Master  
Using pin GPIO2 for button. The digital pin has a pull up resistor so we can
set the pin in INPUT_PULLUP mode. Connect a momentary button between GPIO2 and
GROUND. When released the pin will pull back up to HIGH.

GPIO2 is the pin next to GROUND

Slave  
Using pin GPIO2 for a HIGH/LOW signal, use that signal to trigger a relay. 
Slave will listen for a message telling it to turn on or off using ESP-NOW. The
circuit for this will be more complex. I want to keep the draw through the ESP8266-01 low so I will be using the HIGH/LOW signal to activate a transistor that
will power the relay directly from the battery. I'll probably end up buying a
relay that has this circuit built in.

