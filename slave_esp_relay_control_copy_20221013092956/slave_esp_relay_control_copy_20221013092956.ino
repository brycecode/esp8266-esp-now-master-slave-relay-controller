
#include <ESP8266WiFi.h>
#include <espnow.h>

#define LED 2
 
// REPLACE WITH THE MAC Address of your receiver 
uint8_t broadcastAddress[] = {0x5C, 0xCF, 0x7F, 0x87, 0x60, 0x27};

//Structure example to send data
//Must match the receiver structure
typedef struct struct_message {
    bool state;
} struct_message;

// Create a struct_message called DHTReadings to hold sensor readings
struct_message switchState;

// Create a struct_message to hold incoming sensor readings
struct_message incomingState;
bool relayState = false;


// Callback when data is received
void OnDataRecv(unsigned char* mac, unsigned char* incomingData, unsigned char len) {
  memcpy(&incomingState, incomingData, sizeof(incomingState));
  Serial.print("Bytes received: ");
  Serial.println(len);
  relayState = incomingState.state;
  if (relayState == false) {
    Serial.println("Turning off");
    digitalWrite(LED, LOW);    
  }
  else {
    Serial.println("Turning on");
    digitalWrite(LED, HIGH);
  }
}
 
void setup() {
  // Init Serial Monitor
  Serial.begin(9600);
 
  // Set device as a Wi-Fi Station
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();

  // Init ESP-NOW
  if (esp_now_init() != 0) {
    Serial.println("Error initializing ESP-NOW");
    return;
  }

  // Set ESP-NOW Role
  esp_now_set_self_role(ESP_NOW_ROLE_SLAVE);  

  esp_now_register_recv_cb(OnDataRecv);

  pinMode(LED, OUTPUT);
}

void loop() {
  
}